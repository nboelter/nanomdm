--
-- PostgreSQL database dump
--

-- Dumped from database version 11.12 (Debian 11.12-0+deb10u1)
-- Dumped by pg_dump version 11.12 (Debian 11.12-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: certificates; Type: TABLE; Schema: public; Owner: mdm
--

CREATE TABLE public.certificates (
    serial integer NOT NULL,
    name text,
    ts timestamp without time zone
);


ALTER TABLE public.certificates OWNER TO mdm;

--
-- Name: certificates_serial_seq; Type: SEQUENCE; Schema: public; Owner: mdm
--

CREATE SEQUENCE public.certificates_serial_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.certificates_serial_seq OWNER TO mdm;

--
-- Name: certificates_serial_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mdm
--

ALTER SEQUENCE public.certificates_serial_seq OWNED BY public.certificates.serial;


--
-- Name: commandqueue; Type: TABLE; Schema: public; Owner: mdm
--

CREATE TABLE public.commandqueue (
    id integer NOT NULL,
    uuid uuid,
    certificate_serial integer,
    command text,
    ts timestamp without time zone
);


ALTER TABLE public.commandqueue OWNER TO mdm;

--
-- Name: commandqueue_id_seq; Type: SEQUENCE; Schema: public; Owner: mdm
--

CREATE SEQUENCE public.commandqueue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.commandqueue_id_seq OWNER TO mdm;

--
-- Name: commandqueue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mdm
--

ALTER SEQUENCE public.commandqueue_id_seq OWNED BY public.commandqueue.id;


--
-- Name: commands; Type: TABLE; Schema: public; Owner: mdm
--

CREATE TABLE public.commands (
    id text,
    certificate_serial integer,
    device text,
    command character varying(255),
    ts timestamp without time zone,
    status character varying(255)
);


ALTER TABLE public.commands OWNER TO mdm;

--
-- Name: devices; Type: TABLE; Schema: public; Owner: mdm
--

CREATE TABLE public.devices (
    certificate_serial integer NOT NULL,
    push_token text,
    push_magic text,
    udid text,
    available_device_capacity real,
    build_version text,
    device_capacity real,
    device_name text,
    host_name text,
    local_host_name text,
    model text,
    model_name text,
    os_version text,
    product_name text,
    serial_number text,
    last_cloud_backup_date timestamp without time zone,
    last_update timestamp without time zone
);


ALTER TABLE public.devices OWNER TO mdm;

--
-- Name: profiles; Type: TABLE; Schema: public; Owner: mdm
--

CREATE TABLE public.profiles (
    certificate_serial integer,
    udid text,
    uuid uuid,
    identifier text,
    display_name text,
    is_managed text,
    last_update timestamp without time zone
);


ALTER TABLE public.profiles OWNER TO mdm;

--
-- Name: certificates serial; Type: DEFAULT; Schema: public; Owner: mdm
--

ALTER TABLE ONLY public.certificates ALTER COLUMN serial SET DEFAULT nextval('public.certificates_serial_seq'::regclass);


--
-- Name: commandqueue id; Type: DEFAULT; Schema: public; Owner: mdm
--

ALTER TABLE ONLY public.commandqueue ALTER COLUMN id SET DEFAULT nextval('public.commandqueue_id_seq'::regclass);


--
-- Name: certificates certificates_pkey; Type: CONSTRAINT; Schema: public; Owner: mdm
--

ALTER TABLE ONLY public.certificates
    ADD CONSTRAINT certificates_pkey PRIMARY KEY (serial);


--
-- Name: commandqueue commandqueue_pkey; Type: CONSTRAINT; Schema: public; Owner: mdm
--

ALTER TABLE ONLY public.commandqueue
    ADD CONSTRAINT commandqueue_pkey PRIMARY KEY (id);


--
-- Name: devices devices_pkey; Type: CONSTRAINT; Schema: public; Owner: mdm
--

ALTER TABLE ONLY public.devices
    ADD CONSTRAINT devices_pkey PRIMARY KEY (certificate_serial);


--
-- PostgreSQL database dump complete
--

