#!/usr/bin/env python3
import psycopg2, sys, uuid, plistlib, configparser

config = configparser.ConfigParser()
config.read('nanomdm.ini')

if len(sys.argv) != 3:
    print("Usage: %s # identifier" % sys.argv[0])
    exit()
certificate_serial = sys.argv[1]
identifier = sys.argv[2]
if identifier.endswith(".mobileconfig"):
	identifier = identifier[:-13]

connection = psycopg2.connect(config['default']['dbstring'])
cursor = connection.cursor()

command_uuid = str(uuid.uuid4())

request = plistlib.dumps({'CommandUUID': command_uuid, 'Command': {'RequestType': 'RemoveProfile', 'Identifier': identifier}}).decode()

cursor.execute("SELECT udid FROM devices WHERE certificate_serial = %s", (certificate_serial,))
udid = cursor.fetchone()
if udid == None:
    print("Error: device not found")
    exit()
udid = udid[0]

cursor.execute("INSERT INTO commandqueue (uuid, certificate_serial, command, ts) VALUES (%s, %s, %s, NOW())", (command_uuid, certificate_serial, request));
cursor.execute("INSERT INTO commands (id, certificate_serial, device, command, ts, status) VALUES (%s, %s, %s, %s, NOW(), 'Waiting')", (command_uuid, certificate_serial, udid, 'RemoveProfile'));

connection.commit()
