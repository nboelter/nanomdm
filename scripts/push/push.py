#!/usr/bin/env python3
import configparser
import httpx
import json
import psycopg2
import sys

config = configparser.ConfigParser()
config.read('/home/niklas/mdm/nanomdm.ini')

if len(sys.argv) != 2:
    print("Usage: %s #" % sys.argv[0])
    exit()
certificate_serial = sys.argv[1]

connection = psycopg2.connect(config['default']['dbstring'])
cursor = connection.cursor()

cursor.execute("SELECT push_token, push_magic FROM devices WHERE certificate_serial = %s", (certificate_serial,))
(push_token, push_magic) = cursor.fetchone()
cursor.close()
connection.close()

http_headers = {'apns-topic': config['default']['apns_uid'], 'apns-priority': '10', 'apns-expiration': '0', 'apns-push-type': 'mdm', 'Content-Type': 'application/json'}
with httpx.Client(http2=True, cert=(config['default']['apns_cert'], config['default']['apns_key'], None)) as client:
	response = client.post(f"https://api.push.apple.com/3/device/{push_token}", headers=http_headers, json={'mdm': push_magic})
	print("Status:", response.status_code)
	for key in response.headers:
		print(f"{key}:", response.headers[key])
	print(response.text)
	if response.status_code != 200:
		sys.exit(-1)
