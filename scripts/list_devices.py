#!/usr/bin/env python3
import psycopg2, sys, configparser

config = configparser.ConfigParser()
config.read('nanomdm.ini')
connection = psycopg2.connect(config['default']['dbstring'])
cursor = connection.cursor()
cursor.execute('SELECT certificate_serial, device_name, product_name, udid, last_update FROM devices WHERE NOW() - interval \'1 month\' < last_update ORDER BY certificate_serial ASC')
if '--verbose' in sys.argv:
    for device in cursor.fetchall():
        print(u'%s %s (%s) %s %s' % device)
        
        cursor.execute('SELECT display_name, identifier, is_managed FROM profiles WHERE certificate_serial = %s', (device[0],))
        for profile in cursor.fetchall():
            if profile[2] == 'true':
                print(u' * %s (%s)' % profile[0:2])
            else:
                print(u' * %s (%s) [NOT MANAGED]' % profile[0:2])
else:
    for device in cursor.fetchall():
        print(u'%s %s (%s) %s %s' % device)
