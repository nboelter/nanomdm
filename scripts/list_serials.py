#!/usr/bin/env python3
import psycopg2, sys, configparser

config = configparser.ConfigParser()
config.read('nanomdm.ini')
connection = psycopg2.connect(config['default']['dbstring'])
cursor = connection.cursor()
cursor.execute('SELECT certificate_serial FROM devices ORDER BY certificate_serial ASC')
for device in cursor.fetchall():
    print(device[0])
