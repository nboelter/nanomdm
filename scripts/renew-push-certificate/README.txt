./mdmctl mdmcert.download -new -email=mdmcert@example.org
# Wait for email
./mdmctl mdmcert.download -decrypt=mdm_signed_request.20200821_142138_772.plist.b64.p7
# Upload to https://identity.apple.com/pushcert/

# Put new certificate in mdmcert.download.push.cert

cp mdmcert.download.push.cert ../push/push.cert.pem
cp mdmcert.download.push.key ../push/push.key.pem
