#!/usr/bin/env python3
import psycopg2, sys, configparser
import uuid, plistlib

config = configparser.ConfigParser()
config.read('nanomdm.ini')

if len(sys.argv) != 2:
    print("Usage: %s #" % sys.argv[0])
    exit()
certificate_serial = sys.argv[1]

connection = psycopg2.connect(config['default']['dbstring'])
cursor = connection.cursor()

command_uuid = str(uuid.uuid4())

request = plistlib.dumps({'CommandUUID': command_uuid, 'Command': {'RequestType': 'ProfileList'}}).decode()

cursor.execute("SELECT udid FROM devices WHERE certificate_serial = %s", (certificate_serial,))
udid = cursor.fetchone()
if udid == None:
    print("Error: device not found")
    exit()
udid = udid[0]

cursor.execute("INSERT INTO commandqueue (uuid, certificate_serial, command, ts) VALUES (%s, %s, %s, NOW())", (command_uuid, certificate_serial, request));
cursor.execute("INSERT INTO commands (id, certificate_serial, device, command, ts, status) VALUES (%s, %s, %s, %s, NOW(), 'Waiting')", (command_uuid, certificate_serial, udid, 'ProfileList'));

connection.commit()
