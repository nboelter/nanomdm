#!/usr/bin/python3 -u
import json 
import os
import plistlib
import psycopg2
import re
import uuid

from flask import Flask, Response, request, g
from http import HTTPStatus
from OpenSSL import crypto
from cryptography import x509
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.serialization import PrivateFormat, load_pem_private_key, pkcs12

app = Flask(__name__)

@app.before_request
def before_request():
    g.connection = psycopg2.connect(os.getenv('DBSTRING'))

@app.teardown_request
def teardown_request(exception):
    g.connection.close()

def http_error(str):
    print("Error: " + str)
    return (str + "\r\n", HTTPStatus.BAD_REQUEST)

def next_command(certificate_serial):
    """ Return the next command to the device. """
    cursor = g.connection.cursor()
    cursor.execute('SELECT command FROM commandqueue WHERE certificate_serial = %s ORDER BY id ASC LIMIT 1', (certificate_serial,))
    cmd = cursor.fetchone()
    if cmd:
        return Response(cmd[0], mimetype='application/xml')
    else:
        return ("", HTTPStatus.OK)

def new_enrollment_profile(device_name):

    cursor = g.connection.cursor()
    cursor.execute('INSERT INTO certificates (name, ts) VALUES (%s, NOW()) RETURNING serial', (device_name,))
    certificate_serial = cursor.fetchone()[0]
    cursor.close()
    g.connection.commit()
    if not certificate_serial:
        return

    with open(os.getenv('CA_CERT')) as f:
        ca_cert = crypto.load_certificate(crypto.FILETYPE_PEM, f.read())

    with open(os.getenv('CA_KEY')) as f:
        ca_key  = crypto.load_privatekey(crypto.FILETYPE_PEM, f.read())

    push_topic = os.getenv('PUSH_TOPIC')

    with open(os.getenv('PUSH_CERT')) as f:
        push_cert  = f.read().encode()

    key = crypto.PKey()
    key.generate_key(crypto.TYPE_RSA, 2048)

    cert = crypto.X509()

    cert.get_subject().C = "DE"
    cert.get_subject().CN = device_name

    cert.set_serial_number(certificate_serial)
    cert.gmtime_adj_notBefore(-600)
    cert.gmtime_adj_notAfter(10 * 365 * 24 * 60 * 60)

    cert.add_extensions([
        crypto.X509Extension(b"keyUsage", True, b"Digital Signature"),
        crypto.X509Extension(b"extendedKeyUsage", False, b"TLS Web Client Authentication"),
        crypto.X509Extension(b"subjectKeyIdentifier", False, b"hash", subject=cert),
        crypto.X509Extension(b"authorityKeyIdentifier", False, b"keyid:always", issuer=ca_cert)
    ])

    cert.set_issuer(ca_cert.get_subject())
    cert.set_pubkey(key)
    cert.sign(ca_key, 'sha256')

    # Workaround to get SHA1 MAC for compatibility 
    encryption = (
        PrivateFormat.PKCS12.encryption_builder().
        kdf_rounds(50000).
        key_cert_algorithm(pkcs12.PBES.PBESv1SHA1And3KeyTripleDESCBC).
        hmac_hash(hashes.SHA1()).build(b"apple")
    )
    pfxdata = pkcs12.serialize_key_and_certificates(
        b'Device Identity',
        key.to_cryptography_key(),
        cert.to_cryptography(),
        None,
        encryption
    )

    identity_uuid = str(uuid.uuid4())
    payload_uuid = str(uuid.uuid4())

    profile = {
        'PayloadDisplayName': 'Enrollment Profile',
        'PayloadContent': [{
            'Password': 'apple',
            'PayloadDisplayName': 'Device Identity',
            'PayloadContent': pfxdata,
            'PayloadType': 'com.apple.security.pkcs12',
            'PayloadUUID': identity_uuid,
            'PayloadIdentifier': 'com.apple.security.pkcs12.' + str(uuid.uuid4()),
            'PayloadVersion': 1,
            'PayloadDescription': 'Adds a PKCS#12-formatted certificate'
        }, {
            'CheckOutWhenRemoved': True,
            'PayloadDescription': 'Enrolls with the MDM server',
            'CheckInURL': f'https://{os.getenv("MDM_HOST")}/checkin',
            'PayloadUUID': str(uuid.uuid4()),
            'IdentityCertificateUUID': identity_uuid,
            'Topic': push_topic,
            'PayloadVersion': 1,
            'PayloadDisplayName': '',
            'ServerCapabilities': ['com.apple.mdm.per-user-connections'],
            'AccessRights': 19,
            'ServerURL': f'https://{os.getenv("MDM_HOST")}/connect',
            'PayloadIdentifier': os.getenv("MDM_CLASSPATH") + '.enroll.mdm',
            'PayloadType': 'com.apple.mdm',
            'PayloadOrganization': 'NanoMDM',
            'PayloadScope': 'System'
        }, {
            'PayloadDisplayName': 'Push Certificate',
            'PayloadContent': push_cert,
            'PayloadUUID': str(uuid.uuid4()),
            'PayloadIdentifier': os.getenv("MDM_CLASSPATH") + '.enroll.cert.ca',
            'PayloadType': 'com.apple.security.root',
            'PayloadOrganization': '',
            'PayloadVersion': 1,
            'PayloadDescription': 'Apple Push Certificate'
        }],
        'PayloadType': 'Configuration',
        'PayloadUUID': payload_uuid,
        'PayloadIdentifier': os.getenv("MDM_CLASSPATH") + '.enroll',
        'PayloadScope': 'System',
        'PayloadOrganization': 'NanoMDM',
        'PayloadVersion': 1,
        'PayloadDescription': 'The server can install configuration profiles'
    }
    resp = plistlib.dumps(profile)

    with open(os.path.join(os.getenv('DEPOT'), payload_uuid + '-enroll.mobileconfig'), 'wb') as f:
        f.write(resp)

    return Response(resp, mimetype='application/x-apple-aspen-config')

""" The TLS stuff is handled by the reverse proxy, example nginx config:
    ssl_certificate     ...;
    ssl_certificate_key ...;

    ssl_client_certificate ...;
    ssl_verify_client optional;

    proxy_set_header X-Verified $ssl_client_verify;
    proxy_set_header X-Serial   $ssl_client_serial;

    location /checkin {
        proxy_pass http://localhost:5000/checkin;
    }
    location /connect {
        proxy_pass http://localhost:5000/connect;
    }
"""

@app.route('/', methods=['GET'])
def form():
    enroll_host = os.getenv('ENROLL_HOST')
    return '''<!DOCTYPE html>
<html><body style="text-align:center"><div>
    <h1>NanoMDM</h1>
    <form method="POST" action="https://%s/enroll">
        Passwort<br><input type="text" name="pin" /><br><br>
        Gerätename<br><input type="text" name="name" /><br><br>
        <input type="submit" value="Geräteverwaltung installieren"/>
    </form>
</div></body><html>
        ''' % enroll_host;

@app.route('/enroll', methods=['POST'])
def enroll():
    if request.form['pin'] != os.getenv('ENROLL_PIN') or len(os.getenv('ENROLL_PIN')) < 4:
        return http_error('Unauthorized')

    name = request.form['name']
    if len(name) < 2:
        return http_error('Name too short')

    if not re.compile(r'[0-9a-zA-Z ]*').fullmatch(name):
        return http_error('Name should only contain 0-9 a-z A-Z and space')

    return new_enrollment_profile(name)

@app.route('/checkin', methods=['PUT'])
def checkin():
    """
    Devices will call /checkin when:
     * The MDM profile is installed (Authenticate)
     * The access tokens for APNS change (TokenUpdate)
     * The MDM profile is removed (Checkout)

    """
    if request.headers.get('X-Verified') != 'SUCCESS':
        return http_error("Missing Client Certificate")

    certificate_serial = int(request.headers.get('X-Serial'), 16)

    if certificate_serial < 1:
        return http_error('Invalid certificate serial number')

    body = request.get_data()
    try:
        req = plistlib.loads(body)
    except plistlib.InvalidFileException:
        return http_error('Not a valid plist file.')

    print(request.path, certificate_serial, req)

    if not 'UDID' in req:
        return http_error('Missing parameter: UDID')
    udid = req['UDID'] 
    
    if not 'MessageType' in req:
        return http_error('Missing parameter: MessageType')
    message_type = req['MessageType']

    cursor = g.connection.cursor()

    if message_type == 'Authenticate':
        cursor.execute('DELETE FROM devices WHERE certificate_serial = %s', (certificate_serial,))
        cursor.execute('INSERT INTO devices (certificate_serial, udid) VALUES (%s, %s)', (certificate_serial, udid))

    elif message_type == 'TokenUpdate':
        if not 'Token' in req:
            return http_error('Missing parameter: Token')
        push_token = req['Token'].hex()

        if not 'PushMagic' in req:
            return http_error('Missing parameter: PushMagic')
        push_magic = req['PushMagic']

        cursor.execute('UPDATE devices SET push_token = %s, push_magic = %s WHERE certificate_serial = %s AND udid = %s', 
            (push_token, push_magic, certificate_serial, udid))
   
    elif message_type == 'Checkout':
        cursor.execute('DELETE FROM devices WHERE certificate_serial = %s AND udid = %s', (certificate_serial, udid))
   
    else:
        return http_error('Unknown MessageType')

    cursor.close()
    g.connection.commit()
    return('Success', HTTPStatus.OK)

@app.route('/connect', methods=['PUT'])
def connect():
    """ When a device receives a push notification it will
        call /connect with Status = Idle and receive a command.

        It will then try to execute the command and report back
        with Status = Acknowledged, Error or CommandFormatError.
        It will then receive the next command in the queue.

        If the command cannot be executed at that time, it will
        instead report back with Status = NotNow and we will not
        give it another command. It will then poll later when the
        command can be executed.

        We authenticate devices using the client certificate 
        serial number and make sure only to parse commands that
        we are expecting.

    """
    if request.headers.get('X-Verified') != 'SUCCESS':
        return http_error('Missing Client Certificate')
    
    certificate_serial = int(request.headers.get('X-Serial'), 16)
    
    if certificate_serial < 1:
        return http_error('Invalid certificate serial number')
    
    body = request.get_data()
    try:
        req = plistlib.loads(body)
    except plistlib.InvalidFileException:
        return http_error('Not a valid plist file.')

    print(request.path, certificate_serial, req)
    
    if not 'UDID' in req:
        return http_error('Missing parameter: UDID')
    udid = req['UDID']

    if not 'Status' in req:
        return http_error('Missing parameter: Status')
    status = req['Status']
    
    if status == 'Idle':
        return next_command(certificate_serial)
    
    if status == 'NotNow':
        return ('', HTTPStatus.OK)
    
    if not 'CommandUUID' in req:
        return http_error('Missing Parameter: CommandUUID')

    command_uuid = req['CommandUUID']
    
    cursor = g.connection.cursor()
    
    cursor.execute("SELECT command FROM commands WHERE id = %s AND device = %s AND status = 'Waiting';", (command_uuid, udid))
    command = cursor.fetchone()

    if command != None:
        command = command[0]
    
    # We only accept the response if we are waiting for it!
    if status == 'Acknowledged' and command != None:
        if command == 'DeviceInformation':
            info = req['QueryResponses']
            values = (info.get('AvailableDeviceCapacity', 0.0), info.get('BuildVersion', ''), 
                info.get('DeviceCapacity', 0.0), info.get('DeviceName', ''), 
                info.get('HostName', ''), info.get('LocalHostName', ''), 
                info.get('Model', ''), info.get('ModelName', ''), 
                info.get('OSVersion', ''), info.get('ProductName', ''), 
                info.get('SerialNumber', ''), 
                info.get('LastCloudBackupDate', '1970-01-01'), certificate_serial)
            cursor.execute("""
                UPDATE devices 
                SET available_device_capacity = %s, build_version = %s, device_capacity = %s, 
                    device_name = %s, host_name = %s, local_host_name = %s, model = %s, 
                    model_name = %s, os_version = %s, product_name = %s, serial_number = %s, 
                    last_cloud_backup_date = %s, last_update = NOW() WHERE certificate_serial = %s""", values)

        if command == 'ProfileList':
            info = req['ProfileList']
            cursor.execute("DELETE FROM profiles WHERE certificate_serial = %s;", (certificate_serial,))

            for profile in info:
                values = (certificate_serial, udid, profile['PayloadUUID'], profile['PayloadIdentifier'], 
                    profile['PayloadDisplayName'], profile.get('IsManaged', ''))
                cursor.execute("""
                    INSERT INTO profiles (certificate_serial, udid, uuid, identifier, display_name, is_managed, last_update) 
                    VALUES (%s, %s, %s, %s, %s, %s, NOW())""", values);

    cursor.execute("DELETE FROM commandqueue WHERE certificate_serial = %s AND uuid = %s", (certificate_serial, command_uuid,))
    
    if status in ['Acknowledged', 'Error', 'CommandFormatError'] and command != None:
        cursor.execute("UPDATE commands SET status = %s WHERE id = %s AND device = %s AND status = 'Waiting';", 
            (status, command_uuid, udid))
    else:
        print("Error: Status = %s, Command = %s" % (status, command))
    g.connection.commit()
    cursor.close()
    return next_command(certificate_serial)
